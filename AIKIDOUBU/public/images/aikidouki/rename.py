import os
import glob


jsonname = "photoindex.json"
imageplace = "./images/aikidouki/"

f = open(jsonname,'w')

#change filename to directory name fprefix+sequentialnumber.
def changeName(dir,index):
    print(">"+dir)
    dirnameT=os.path.split(dir)
    prefixname=dirnameT[len(dirnameT)-1]
    print("dirname="+prefixname)
    for eachfile in glob.glob(dir+'/*') :
       if os.path.isdir(eachfile):
           changeName(eachfile,0)
       else:
           path,ext=os.path.splitext(os.path.basename(eachfile))
           fname = prefixname+'_'+str(index)+ext
           newname=os.path.join(dir,fname)
           index=index+1
           print(eachfile,"-->",newname)
           os.rename(eachfile,newname)
           info ='{ "category":'+prefixname+','+'"dataUrl":"'+ imageplace+prefixname+"/"+fname+'"},'+str(os.linesep)
           print(info)
           f.write(info)



if __name__ == '__main__':
    parentdir='.'
    absparentdir=os.path.abspath(parentdir)
    print(absparentdir)
    start = "["+os.linesep
    f.write(start)
    for dir in glob.glob(absparentdir+'/*'):
        if os.path.isdir(dir):
            changeName(dir,0)

    f.write(']')
