var express = require('express');
var router = express.Router();

// 写真のサンプルデータ
//var photoList = require('./aikiphotoindex.json');

var fs = require('fs');
var photoList = JSON.parse(fs.readFileSync('public/images/aikidouki/photoindex.json','utf8'));




/* GET home page. */
/*
router.get('/', function(req, res, next) {

    res.render('aikidouki', {title: 'Aikidousou', photolist: photoList});
    /*
    require('connect-ensure-login').ensureLoggedIn(),
        function (req, res) {
 res.render('aikidouki', {title: 'Aikidousou', photolist: photoList});

        }

});
*/
router.get('/',
    require('connect-ensure-login').ensureLoggedIn(),
    function(req, res){
        res.render('aikidouki', {title: 'Aikidousou', photolist: photoList});
    });


function isAuthenticated(req, res, next){
    console.log("isAuth in aikidouki!");
    if (req.isAuthenticated()) {  // 認証済
	//res.render('aikidouki', { title: 'Aikidousou', photolist: photoList });
	console.log("YES");
	return next();
    }
    else {  // 認証されていない
	console.log("REDIRECT!");
	res.redirect('/login');  // ログイン画面に遷移
    }
}



/* GET photoList. */
router.get('/photo/list',function(req,res,next){
    res.json(photoList);
});


router.get('/photo/:photoId',function(req,res,next){
    var photo;
    for (i=0; i < photoList.length; i++){
	if(photoList[i].id == req.params.photoId){
	    var photo = photoList[i];
	}
    }
    res.json(photo);
});



module.exports = router;
