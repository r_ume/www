# node.js インストール


# nodeを直接作る
package.jsonファイルを生成します。
$ mkdir new_project
$ cd new_project/
$ npm init

http://localhost:3000がデフォルト



# expressを使う
## express  インストール
npm install express --save

## express テンプレート ejs
npm install ejs --save

## express directory インストール
npm install -g express-generator

express -e YOUR_PROJECT
$ express -e YOUR_PROJECT
$ cd YOUR_PROJECT/
$ ls
app.js      bin     package.json    public      routes      views
上記のようにプロジェクトディレクトリの配下にいくつかのファイルとディレクトリが作成されます。それぞれの役割は下記の通りです。
• app.js: このアプリのメインファイル。アプリ全体・すべてのリクエストに共通する設定・処理を記述します。
• bin: サーバープロセスの起動処理などを格納するディレクトリ。デフォルトではwwwファイルが生成されており、3000番ポートでサーバープロセスを起動するようになっています。
• package.json: このプロジェクトの名前、バージョン、依存関係などを定義する設定ファイル。
• public: 静的リソース（Javascript, CSS, イメージファイルなど）を格納するディレクトリ。
• routes: ルーティング処理を格納するディレクトリ。ルーティングとはクライアントが要求するURIに応じてどのような処理を実行するかを返すかを振り分ける処理のことです。
• views: UIを格納するディレクトリ。コンテンツは選択したテンプレートエンジンに対応したフォーマットで記述していきます。EJSであればindex.ejsのように拡張子を設定します。
この中で私がExpressの肝だと思うのがapp.jsとroutesの使い分けです。routesはルーティングを格納するためのディレクトリとして位置付けられていますが、ごく小規模なものであればapp.jsにすべて記述することも不可能ではありません。

## express構造下 サーバー立ち上げ
./bin/www

## 足りないモジュールを自動追加

npm install npm-install-missing -g
npm-install-missing
(エラーとなる場合あり)

rm -rf node_modules
npm cache clear
npm install




# node.js jsonデータ取り込み
[
  {
      "daynum": "1",
      "dishname": "おいしいカレー",
      "description": "みんな好きなカレーです。甘口です。",
      "item": [
        {
        "name": "にんじん",
        "amount": "中3本"
        },
	{
	  "name": "たまねぎ",
	  "amount": "中2玉"
	}
      ]
  }
]

var xx = require("./data.json"); //大量のJSONデータを読み込むとメモリーリークを起こす場合がある。
var obj = JSON.parse(fs.readFileSync('filepath', 'utf8'));

filepath /publicからの相対パス


# 認証情報設定 passport利用
