var express = require("express");
var app = express();


var server = app.listen(3000,function(){
    console.log("Node.js is listening to PORT:" + server.address().port);
});

app.use(express.static('aiki'));

// 写真のサンプルデータ
var photoList = [
        {
	    id:"001",
	    name: "2009010401.jpg",
	    type: "jpg",
	    dataUrl:"http://localhost:3000/images/2009010401.jpg"
	},
        {
	    id:"002",
	    name: "2009010402.jpg",
	    type: "jpg",
	    dataUrl:"http://localhost:3000/images/2009010402.jpg"
	},
        {
	    id:"003",
	    name: "2009010403.jpg",
	    type: "jpg",
	    dataUrl:"http://localhost:3000/images/2009010403.jpg"
	},
        {
	    id:"004",
	    name: "2009010404.jpg",
	    type: "jpg",
	    dataUrl:"http://localhost:3000/images/2009010404.jpg"
	},
        {
	    id:"005",
	    name: "2009010405.jpg",
	    type: "jpg",
	    dataUrl:"http://localhost:3000/images/2009010405.jpg"
	},
        {
	    id:"006",
	    name: "2009010406.jpg",
	    type: "jpg",
	    dataUrl:"http://localhost:3000/images/2009010406.jpg"
	},
        {
	    id:"007",
	    name: "2009010407.jpg",
	    type: "jpg",
	    dataUrl:"http://localhost:3000/images/2009010407.jpg"
	}

]

app.get("/aiki/photo/list",function(req,res,next){
    res.json(photoList);
});

app.get("/aiki/photo/:photoId",function(req,res,next){
    var photo;
    for (i=0; i < photoList.length; i++){
	if(photoList[i].id == req.params.photoId){
	    var photo = photoList[i];
	}
    }
    res.json(photo);
});


app.set('view engine','ejs');

app.get("/",function(req,res,next){
    res.render("index",{});
});


